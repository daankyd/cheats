# AngularJS 1
<https://angularjs.org>  
<https://docs.angularjs.org/api> Start typing in the searchbar  
[John papa angular style guide](https://github.com/johnpapa/angular-styleguide/blob/master/a1/README.md)  
<https://egghead.io/technologies/angularjs>  
I try to use the John papa style guide for angular.


### Start angular app

Link angular lib in your HTML  
Link your local angular app.js in the bottom of the page    
```

    <p>bla</p> 
    
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>
    <script src="dist/js/app.js"></script>
    <!-- don't forget to link alle modules to the html dist/js/controllers/someController.js -->
</body>    
```

app.js file
```
// to set a module
angular.module('appName', []);

<HTML ng-app="appName">


// to get a module
angular.module('appName');
```

### Expressions
```
<p>I am {{ 4 + 6 }}</p> =>  <p>I am 10</p>
<p>I am {{ 'daan' + ' schaeffer' }}</p> =>  <p>I am daan schaefer</p>
```

**Everything in an expression is Java Script!!**

### Controller
Use for every controller a separate js file
```
(function(){
    // use a getter
    angular.module('appName')
           .controller('SomeController',functionName);
    function functionName(){
        var that = this;
        that.name = 'Daan'; 
    }
})();
```
HTML
```
<div ng-controller="SomeController as person">
    {{person.name}}
</div>
```

### directives 
**Built-in**  
<https://docs.angularjs.org/api/ng/directive>

* ng-show="js" ng-show="itemIndex === 2"  
* ng-hide="js"
* ng-repeat="product in store.products"
* ng-src ```<img ng-src="{{img.thumb}}">```
* ng-click=" itemIndex = 2 " 
* ng-init=" itemIndex = 1 " (default val) 
* ng-class="expression" (expression => myClass; var myClass='border'; ```<el class='border'>```)
* ng-class="{classname:store.hasProperty}" => object; key = classname, val is expression to  true of false (show, hide)
* ng-model
* ng-options ```<select ng-options="stars for stars in [3,2,1]>```
* ng-submit="scope.submit()" in form eleent
* ng-include="'template-name.html'"

**Custom directives**
```
// Element Directive
<product-title></product-title>
app.directive('productTitle',function(){
    return { // configuration object to define directve
        restrict:'E', // E=element
        templateUrl: 'product-title.html' // template: '<h1>{{product.title}}'
    }
});

// Attribute Directive
<h3 product-title></h3>
app.directive('productTitle',function(){
  return { // configuration object to define directve
      restrict:'A', // A=attribute
      templateUrl: 'product-title.html' // template: '<h1>{{product.title}}'
  }
});

// add a controller in a custom directive
app.directive('productTitle',function(){
    return { // configuration object to define directve
        restrict:'E', // E=element
        templateUrl: 'product-title.html' // template: '<h1>{{product.title}}',
        controller: function(){
            this.title='ola';
        },
        controllerAs:'product'
    }
});

```




### filters
<https://docs.angularjs.org/guide/filter>  
**Built-in**  
<https://docs.angularjs.org/api/ng/filter>  

* {{product.price | currency:options}}
* | limtTo
* | orderBy
* | date
* | number
* | filter (deep search filter by value)

### Dependency injection
```
module.controller('ControllerController',['$route','dependencyModule',function($route,dependencyModule){
    
}];
```

### Form validation
```
<form name="formName' ng-submit="formName.$valid && controllerCtrl.submitFunc()" novalidate >
{{formName.$valid}}

<input required class='dynamic updated!!'> => ng-pristine ng-invalid ng-diry
Works for: type="email", type="url", type="number" min=1 max=4


```

### Modules
Seperate modules based on groups of functionality  
angular.module('panels',[]);  
Inject it in the main app  
angular.module('appName',['panels']);  

### Services
**Built-in**  
<https://docs.angularjs.org/api/ng/service>

* $http
* $log
* $filter

#### $http
```
angular.module('appName').controller('myController',['$http',function($http){
    var that=this;
    that.results=[];
    $http.get('/someUrl.json').then(function(data){
        that.results=data;    
    });
}]);

```