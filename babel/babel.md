# Babel, a JavaScript compiler/ transpiler
This is in a Grunt context. It can also be done in NPM Scripts or Gulp.

* [babeljs.io/](https://babeljs.io/)

### Install and setup

Create the folowing files

* package.json (use _npm init_ to create one)
* gruntfile.js 
* .babelrc
* js/es6.js
* index.html


```bash
npm init
npm install --save-dev grunt
npm install --save-dev grunt-babel
npm install --save-dev babel-preset-es2015
```

### Configure

A minimal gruntfile looks like

```javascript
module.exports = function(grunt) {
    "use strict";

    // Project configuration.
    grunt.initConfig({
        babel:{
            options: {
                sourceMap: true
            },
            dist: {
                files:{
                    'dist/js/es5.js': 'js/es6.js'
                }
            }
        }
    });


    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-babel');

    // Default task(s).
    grunt.registerTask('default', ['babel']);
};

```

include a _.babelrc_ file and add a preset.

```javascript
{
    "presets": ["es2015"]
}
```

And finaly create the html (with emmet create a boilerplate)
```
html:5 
```

### polyfill
Or use the babel polyfill
this replaces the transpiler, so you can use es6 code
```
npm install --save-dev babel-polyfill
<script src="node_modules/babel-polyfill/dist/polyfill.min.js"></script>
<script src="./js/es6.js"></script>
```