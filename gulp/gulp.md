# Gulp js
[gulpjs.com](http://gulpjs.com/)  
https://github.com/gulpjs/gulp/blob/master/docs/README.md#articles  

### Install Gulp glonbaly, only once
```
$ npm install gulp -g
```

### Setup project 
```
// create Node project (package.json)
$ npm init

// install Gulp in project
$ npm install gulp -save-dev

// create Gulpfile
$ touch gulpfile.js

// testing
$ gulp
// should return "task default was not defined..."

```

### Add packages
```
$ npm i gulp-less -save-dev
```

### Create a default Gulp function
open the Gulpfile gulpfile.js

```
var gulp=require('gulp'),
	less=require('gulp-less');

gulp.task('process-less',function(){
	return gulp.src('./lessFilename.less')
		.pipe(less({}))
		.pipe(gulp.dest('./dest/''));
});

gulp.task('default',function(){
	console.log('this is a configured gulpfile');
});
``` 

