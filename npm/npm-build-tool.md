# How to use npm as a Build Tool

Here i try to use npm to convert md to html

[https://docs.npmjs.com/misc/scripts](https://docs.npmjs.com/misc/scripts)

[https://docs.npmjs.com/misc/scripts](https://docs.npmjs.com/misc/scripts)

## Start npm

To create a package.json run:
```
> npm init
```

npm install <package_name> --save-dev

## Use script

In the package.json file there is the scirpts directive where you can declare your scripts to run.

```javascript
{
  "name": "myproject",
  "devDependencies": {
    "jshint": "latest",
    "browserify": "latest",
    "mocha": "latest"
  },
  "scripts": {
    "lint": "jshint **.js",
    "test": "mocha test/",
    "env": "env"
  }
}
```

To find env vars which you can use 
```javascript
"scripts": {
    "env": "env"
}
npm run env
```
To use in a script
```javascript
"scripts": {
    "print env": "echo $npm_package_description"
}
```
## Run the script!!
```
 npm run scriptname
```

## Shortcuts

npm also provides a few convinient shortcuts. The npm test, npm start, npm stop commands are all shortcuts for their run equivalents, e.g. npm test is just a shortcut for npm run test.

## Pre and Post Hooks

Another cool feature about npm is that any script that can be executed also has a set of pre- and post- hooks, which are simply definable in the scripts object. For example, if you execute npm run lint, despite npm having no preconceived idea of what the lint task is, it will immediately run npm run prelint, followed by npm run lint, followed by npm run postlint. The same is true for any command, including npm test (npm run pretest, npm run test, npm run posttest). The pre and post scripts are also exit-code-sensitive, meaning if your pretest script exits with a non-zero exit code, then NPM will immediately stop, and not run the test and posttest scripts. You can't pre- a pre- script though, so prepretest gets ignored. npm also runs the pre- and post- hooks for a few internal commands: install , uninstall, publish, update.

```
  "scripts": {
    "test": "mocha test/",
    "pretest": "echo 'before test'",
    "posttest": "echo 'after test'"
  }

OR go extreme

  "scripts": {
    "lint": "jshint **.js",
    "build": "browserify index.js > myproject.min.js",
    "test": "mocha test/",

    "prepublish": "npm run build # also runs npm run prebuild",    
    "prebuild": "npm run test # also runs npm run pretest",
    "pretest": "npm run lint"
  }
```
## Usefull bash syntax

* && for chaining tasks
* & for running tasks simaltaneously
* < for inputting the contents (stdin) of a file to a command
* > for redirecting output (stdout) of a command and dumping it to a file
* | for redirecting output (stdout) of a command and sending it to another command

```
npm view versions 
```