#webpack 2 (https://webpack.js.org/)
Module bundler


npm rimraf   
This is for all filesystems  
npm scripts:  
"build": "rimraf dist && webpack src/index.js dist/bundle.js"

make a watch in npm scripts  
add '--watch'
"build": "rimraf dist && webpack src/index.js dist/bundle.js --watch"

*** config file
'webpack.config.js' this is de default settings file. You dont need to specify this.
Otherwise use 'webpack --config webpack-custom.cofig.js' or somthing like that.

```
var path = require("path");

module.export= {
    entry: "src/index.js",
    output: {
        path: path.join(__dirname,'dist'), // this way it is file system independent (pc/linux/whatever)
        publicPath: '/', // where is it found on the server
        filename: 'bundle.js'
    },
    module:{ // the loaders!
        loaders:[
            {
            test: /\.js?$/,
            loaders: ['babel-loader'],
            exclude: '/node_modules/'
            }
        ]
        
    }
}
```


### Hot module replacement
This i skip for now

### webpack-dev-server
this is for development, it builds a bundle in memory, so it not build in the distfolder but in memory and pusht to the server.

### environment variables
(I hope it works on windows)  
In your npm script tag 'NODE_ENV=production' or somthing like that  
Then in webpack.config get read it through 'process.env.NODE_ENV'   
The you can use the define plugin to define it for the use of a module!!  

```

// package.json npm sctript
    "build": "rimraf dist && NODE_ENV=production webpack",
    "dev": "NODE_ENV=development webpack-dev-server",


// webpack.config.js
const development = process.env.NODE_ENV === development
const production = process.env.NODE_ENV === production

let plugins = development ?
 [new webpack.pluginForDev] :
 [new webpack.pluginForProd1,new webpack.pluginForProd2];
 
 plugins.push(new webpack.DefinePlugin({
 DEFELOPMENT: JSON.stringify(development),
 PRODUCTION: JSON.stringify(production),
 }));

// in a js module:
if(DEFELOPMENT){
 do somthing for dev only!
}

```

### sourcemap
add Sourcemaps for your js  
devtool: 'source-map'

```
var path = require("path");

module.export= {
    devtool: 'source-map',
    entry: "src/index.js",
    output: {
        path: path.join(__dirname,'dist'), // this way it is file system independent (pc/linux/whatever)
        publicPath: '/', // where is it found on the server
        filename: 'bundle.js'
    },
    module:{ // the loaders!
        loaders:[
            {
            test: /\.js?$/,
            loaders: ['babel-loader'],
            exclude: '/node_modules/'
            }
        ]
        
    }
}
```

### images
Use a file-loader  // this loads the file with an hashname and copied that
use a url-loader // this loads small images as a data-url insode the html to prevent an extra servercall
```

// your js module file
const image = require('img/image.jpg');
export default `<img src="${image}" >`;

// your webpack config file
    module:{ // the loaders!
        loaders:[
            {
                test: /\.js?$/,
                loaders: ['babel-loader'],
                exclude: '/node_modules/'
            },
            {
                test: /\.(jpg|png|gif)?$/,
                <!-- loaders: ['file-loader'], -->
                loaders: ['url-loader?limit=10000&name=images/[hash:12']:[ext],
                exclude: '/node_modules/'
            },
                        
        ]


```

### trees shaking
dead code elimination!  
use plugin new webpack.optimize.UglifyJsPlugin() // only for prod
```

// webpack.config.js
const development = process.env.NODE_ENV === development
const production = process.env.NODE_ENV === production

let plugins = development ?
 [new webpack.pluginForDev] :
 [
    new webpack.pluginForProd1,
    new webpack.pluginForProd2,
    new webpack.optimize.UglifyJsPlugin()
   ];
 
 plugins.pust(new webpack.pluginForALL);

```

### styling 

- css-loader // this load the css from the css file
- style-loader // this loads the styles in the head of the page
- extract-text-webpack-plugin // this gets the styles from the head of the page and put it in a seperate file


// add an event
document.hetelementById('idName').addEventListener('click',() = > {});