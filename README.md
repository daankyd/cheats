# Cheat cheats / notes

Here are my notes i made for several techniques.
Most of them are noted while whatching a course.

### Markdown

Trie to use [Markdown MD](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
## webpack 2
* [setup webpack ernv](./webpack2/webpack2.md)

## Javascript 2015, ES6, ECMAScript 2015
* [Language Specificaton](./es6/es6.md)

## Babel, JavaScript compiler
* [Babel](./babel/babel.md)

## NPM
* [How to use npm as a Build Tool](./npm/npm-build-tool.md)

## GULPjs
* [Setup a Gulp environmet](./gulp/gulp.md)

## AngularJS 1
* [Angular cheat](./angular1/angular.md)

## Chrome devtools
* [notes for Chrome Devtools](./devtools/devtools.md)

## Legacy

There are some sheets i made prior to git and md

* [angular basics](./legacy/angular-basics.html)
* [angular setup](./legacy/angular-setup.html)
* [bootstrap bower grunt](./legacy/bootstrap-bower-gunt.html)
* [css](./legacy/css)
* [jquery](./legacy/jquery.html)
* [js](./legacy/js.html)
* [sublimetext2](./legacy/sublimetext2.html)
* [terminal](./legacy/terminal.html)
* [wordpress](./legacy/wordpress.html)
* [workflow](./legacy/workflow.html)


```
#!javascript

Test
```
een extern toetsenbord zou handig zijn
iPhone