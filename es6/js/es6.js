"use strict";

(function(){
    if(true){
        let scoped_in_this_block = 'scoped';
        console.log(scoped_in_this_block);

    }
    // console.log(scoped_in_this_block);


    const PI = 3.1415;

    let obj = {a:1};
    const cobj = obj;
    cobj.a=2;
    console.log(cobj);

    //default vars
    function someFunc(param='defaultparam', somthingElse = param + ' toevoeging'){
        console.log(param, somthingElse);

    }
    someFunc();
    someFunc('jajaja');
    someFunc( undefined, 'jajaja');

    //spread
    let nums = [1,2,3];
    function sum(a,b,c){
        return a+b+c;
    }
    console.log( sum(...nums) );
    let arr = [...nums,4,5,6];
    console.log(arr);

    // rest param
    function joinStuff(...things){
        console.log(things.join(' '));
    }
    joinStuff('a',2,3,'komt ie',...nums,'haha');


    // linebreaks
    var template = `This is a fine
    line ;)`;
    console.log(template);

    // interpolation with data

    let a = 'iets',
        b = 'anders';
    console.log( ` soms doe je ${a} heel ${b}, zelfs ${sum(2,2,2)}`);

    function str(a,...rest){
        console.log(a,rest)
    }
    str` soms doe je ${a} heel ${b}, zelfs ${sum(2,2,2)}`;

    var testString='start and end';
    console.log(
        testString.startsWith('start'),
        testString.endsWith('end'),
        testString.includes('and'));

    console.log(testString.repeat(20));


    console.log(Array.of('test',[2,3],{what:'Ever'}));



    let testArr=[
        {name:'daan',last:'schaeffer'},
        {name:'jop', last:'schaeffer'}
    ]

    function findByName(name){
        return testArr.find(function(item){
            return item.name === name;
        });
    }
    console.log( findByName('jop') );

    function findByIndex(name) {
        return testArr.findIndex(function (item) {
            return item.name === name;
        });
    }
    console.log( findByIndex('jop') );

    console.log( testArr.copyWithin(0,1) );

{
    let a = 'test',
        b = 33,
        myObj = {a, b}
    console.log(myObj);

    let objectWithMethod = {
        someMethod (param){
            console.log (param);
        },
        [a+'123']:'testing'
    }
    objectWithMethod.someMethod('yes-joh');
    console.log(objectWithMethod.test123);

    let newObject = Object.assign({},objectWithMethod,myObj,{test:889});
    console.log(newObject);


}

})();

// set
(function(){
    let mySet = new Set();

    let anArray = [1,1,2,3,3,3,3,3,4,5];
    let otherSet = new Set(anArray);

    console.log(otherSet,otherSet.size);
    otherSet.add(9).add(12);
    otherSet.delete(3);
    otherSet.has(1);// bool
    // otherSet.clear();

    otherSet.forEach(function(val1,val2,set){
        //val1 = val;
        //val2 should be the index but in the set it is also val1
        console.log(val1,val2,set);
    });
})();

// map


(function(){
    var map = new Map();
    let a = [3,4];
    let map2 = new Map([ ['key_','val_'],['name','daan'],[1,1], a ]) ;
    map2.set('test', {t:'est'});
    map2.get('naam');
    map2.has('key_');


    console.log(map2,map2.get('name'));
})();

(function(){

    function* vierkant(x) {
        yield x * x;
        yield 'vierkant';
    }

    let ff = vierkant(5);
    console.log(ff.next());
    console.log('somthing else');
    console.log(ff.next());

})();


(function(){
    class Person {
        constructor(name){
            this.name = name;
        }
        sayHello() {
            return `hello, my name is ${this.name}, whats yours?`;
        }
        static upperName (pers){
            pers.name = pers.name.toUpperCase();
        }
    };
    let daan = new Person('Daan');

    console.log(daan.sayHello());
    Person.upperName(daan);
    console.log(daan.sayHello());

    class Father extends Person{
        sayHello() {
            return super.sayHello() + ' im your father?';
        }
    };
    let daanPa = new Father('Vaderrrr');
    console.log(daanPa.sayHello());
    Father.upperName(daanPa);
    console.log(daanPa.sayHello());

})();

(function(){
    var [x,y] = ['a','b'];
    console.log(x,y);
    let {a,b} = {a:'c',b:'d'}
    console.log(a,b);

    let {name,status:{staat}} = {
        name : "Daan Schaeffer",
        status : {
            nationaliteit : "nederlandsche",
            staat : "samenwonend"
        }
    }
    console.log(name, staat); // Daan Schaeffer samenwonend
})();