"use strict";

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _templateObject = _taggedTemplateLiteral([' soms doe je ', ' heel ', ', zelfs ', ''], [' soms doe je ', ' heel ', ', zelfs ', '']);

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _taggedTemplateLiteral(strings, raw) { return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

(function () {
    if (true) {
        var scoped_in_this_block = 'scoped';
        console.log(scoped_in_this_block);
    }
    // console.log(scoped_in_this_block);

    var PI = 3.1415;

    var obj = { a: 1 };
    var cobj = obj;
    cobj.a = 2;
    console.log(cobj);

    //default vars
    function someFunc() {
        var param = arguments.length <= 0 || arguments[0] === undefined ? 'defaultparam' : arguments[0];
        var somthingElse = arguments.length <= 1 || arguments[1] === undefined ? param + ' toevoeging' : arguments[1];

        console.log(param, somthingElse);
    }
    someFunc();
    someFunc('jajaja');
    someFunc(undefined, 'jajaja');

    //spread
    var nums = [1, 2, 3];
    function sum(a, b, c) {
        return a + b + c;
    }
    console.log(sum.apply(undefined, nums));
    var arr = [].concat(nums, [4, 5, 6]);
    console.log(arr);

    // rest param
    function joinStuff() {
        for (var _len = arguments.length, things = Array(_len), _key = 0; _key < _len; _key++) {
            things[_key] = arguments[_key];
        }

        console.log(things.join(' '));
    }
    joinStuff.apply(undefined, ['a', 2, 3, 'komt ie'].concat(nums, ['haha']));

    // linebreaks
    var template = 'This is a fine\n    line ;)';
    console.log(template);

    // interpolation with data

    var a = 'iets',
        b = 'anders';
    console.log(' soms doe je ' + a + ' heel ' + b + ', zelfs ' + sum(2, 2, 2));

    function str(a) {
        for (var _len2 = arguments.length, rest = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
            rest[_key2 - 1] = arguments[_key2];
        }

        console.log(a, rest);
    }
    str(_templateObject, a, b, sum(2, 2, 2));

    var testString = 'start and end';
    console.log(testString.startsWith('start'), testString.endsWith('end'), testString.includes('and'));

    console.log(testString.repeat(20));

    console.log(Array.of('test', [2, 3], { what: 'Ever' }));

    var testArr = [{ name: 'daan', last: 'schaeffer' }, { name: 'jop', last: 'schaeffer' }];

    function findByName(name) {
        return testArr.find(function (item) {
            return item.name === name;
        });
    }
    console.log(findByName('jop'));

    function findByIndex(name) {
        return testArr.findIndex(function (item) {
            return item.name === name;
        });
    }
    console.log(findByIndex('jop'));

    console.log(testArr.copyWithin(0, 1));

    {
        var _a = 'test',
            _b = 33,
            myObj = { a: _a, b: _b };
        console.log(myObj);

        var objectWithMethod = _defineProperty({
            someMethod: function someMethod(param) {
                console.log(param);
            }
        }, _a + '123', 'testing');
        objectWithMethod.someMethod('yes-joh');
        console.log(objectWithMethod.test123);

        var newObject = Object.assign({}, objectWithMethod, myObj, { test: 889 });
        console.log(newObject);
    }
})();

// set
(function () {
    var mySet = new Set();

    var anArray = [1, 1, 2, 3, 3, 3, 3, 3, 4, 5];
    var otherSet = new Set(anArray);

    console.log(otherSet, otherSet.size);
    otherSet.add(9).add(12);
    otherSet.delete(3);
    otherSet.has(1); // bool
    // otherSet.clear();

    otherSet.forEach(function (val1, val2, set) {
        //val1 = val;
        //val2 should be the index but in the set it is also val1
        console.log(val1, val2, set);
    });
})();

// map

(function () {
    var map = new Map();
    var a = [3, 4];
    var map2 = new Map([['key_', 'val_'], ['name', 'daan'], [1, 1], a]);
    map2.set('test', { t: 'est' });
    map2.get('naam');
    map2.has('key_');

    console.log(map2, map2.get('name'));
})();

(function () {
    var _marked = [vierkant].map(regeneratorRuntime.mark);

    function vierkant(x) {
        return regeneratorRuntime.wrap(function vierkant$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.next = 2;
                        return x * x;

                    case 2:
                        _context.next = 4;
                        return 'vierkant';

                    case 4:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _marked[0], this);
    }

    var ff = vierkant(5);
    console.log(ff.next());
    console.log('somthing else');
    console.log(ff.next());
})();

(function () {
    var Person = function () {
        function Person(name) {
            _classCallCheck(this, Person);

            this.name = name;
        }

        _createClass(Person, [{
            key: 'sayHello',
            value: function sayHello() {
                return 'hello, my name is ' + this.name + ', whats yours?';
            }
        }], [{
            key: 'upperName',
            value: function upperName(pers) {
                pers.name = pers.name.toUpperCase();
            }
        }]);

        return Person;
    }();

    ;
    var daan = new Person('Daan');

    console.log(daan.sayHello());
    Person.upperName(daan);
    console.log(daan.sayHello());

    var Father = function (_Person) {
        _inherits(Father, _Person);

        function Father() {
            _classCallCheck(this, Father);

            return _possibleConstructorReturn(this, Object.getPrototypeOf(Father).apply(this, arguments));
        }

        _createClass(Father, [{
            key: 'sayHello',
            value: function sayHello() {
                return _get(Object.getPrototypeOf(Father.prototype), 'sayHello', this).call(this) + ' im your father?';
            }
        }]);

        return Father;
    }(Person);

    ;
    var daanPa = new Father('Vaderrrr');
    console.log(daanPa.sayHello());
    Father.upperName(daanPa);
    console.log(daanPa.sayHello());
})();

(function () {
    var x = 'a';
    var y = 'b';

    console.log(x, y);
    var _a$b = { a: 'c', b: 'd' };
    var a = _a$b.a;
    var b = _a$b.b;

    console.log(a, b);

    var _name$status = {
        name: "Daan Schaeffer",
        status: {
            nationaliteit: "nederlandsche",
            staat: "samenwonend"
        }
    };
    var name = _name$status.name;
    var staat = _name$status.status.staat;

    console.log(name, staat);
})();
//# sourceMappingURL=es5.js.map
