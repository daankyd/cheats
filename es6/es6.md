# ECMAScript® 2015 Language Specification
What changed in ES6

* [ecma-international](http://www.ecma-international.org/ecma-262/6.0/index.html)
* [babel](https://babeljs.io/)
* [es6-features.org/](http://es6-features.org/)

### New scope 
Let and const are block-scoped variables
With the _let_ keyword you can create a _var_ with blockscope.

```javascript
(function(){
    if(true){
        let scoped_in_this_block = 'scoped';
        console.log(scoped_in_this_block);

    }
    console.log(scoped_in_this_block);
})();
/** will output:
scoped
Uncaught ReferenceError: scoped_in_this_block is not defined
*/
```

The _const_ variable let you create constants (also known as "immutable variables"). This is a var wich cannot be changed after asign.
Exept for a reference, the content of an object by reference can be changed.

```javascript
const PI = 3.1415;

let obj = {a:1};
const cobj = obj;
cobj.a=2;

```

### Parameter features and Spread
* Default parameters
* Rest parameters
* Spread operator  ( ...varName )

```javascript
//default
function someFunc(param='defaultparam', somthingElse = param + ' toevoeging'){
    console.log(param, somthingElse);
    
}
someFunc();
someFunc('jajaja');
someFunc( undefined, 'jajaja');

//spread
let nums = [1,2,3];
function sum(a,b,c){
    return a+b+c;
}
console.log( sum(...nums) );
let arr = [...nums,4,5,6];
console.log(arr);

// rest param
function joinStuff(...things){
console.log(things.join(' ');
}
joinStuff('a',2,3,'komt ie',...nums,'haha');
```

### Templating string literal (with backticks) `
* Multiline
* Expression interpolation 
```Javascript
// linebreaks
var template = `This is a fine
line ;)`;
console.log(template);

// interpolation with data

let a = 'iets',
    b = 'anders';
console.log( ` soms doe je ${a} heel ${b}, zelfs ${sum(2,2,2)}`);

// this goes a litle byond the scope, but take a look
   function str(a,...rest){
        console.log(a,rest)
    }
    str` soms doe je ${a} heel ${b}, zelfs ${sum(2,2,2)}`;
```

### New string methods
* startsWith('strart')
* endsWith('end')
* includes('str')
* repeat(3)
```javascipt
var testString='start and end';
console.log(
    testString.startsWith('start'),
    testString.endsWith('end'),
    testString.includes('and'));
console.log(testString.repeat(20));
testString.startsWith('start',2);

```
### Updates to Number Object
These methods are all static, some are updated

* Number.isFinite(1)
* Number.isInfinite(1)
* Number.isInteger(2)
* Number.isSafeInteger()
* Number.isNan()
* Number.parseInt()
* Number.parseFloat()
* EPSILON
* MAX_SAFE_INTEGER
* MIN_SAFE_INTEGER

Number.EPSILON 
Is the difference from one and the smalest number greater then one.

Number.MAX_SAFE_INTEGER
The largest integer which you can use safe in Javascript

### New Methods of the Array Object
* Array.of('test',[2,3],{what:'Ever'})
* Array.from(arrayLike)
* find
* findIndex
* copyWithin()

array.find(function(){})
can find if an property of an object is inside an array and return the first found occurrence.

```javascript
console.log(Array.of('test',[2,3],{what:'Ever'}));

let testArr=[
{name:'daan',last:'schaeffer'},
{name:'jop',last:'schaeffer'}
]

function findByName(name){
    testArr.find(function(item){
        return item.name === name;
    });
}
console.log(findByName('jop'));

function findByIndex(name) {
    return testArr.findIndex(function (item) {
        return item.name === name;
    });
}
console.log( findByIndex('jop') );

console.log( testArr.copyWithin(1,1) );

```

### defining literals
```javascript
var a = 'test',
    b = 33,
    myObj = {a,b}
console.log(myObj );

let objectWithMethod = {
    someMethod (param){
        console.log (param);
    },
     [a+'123']:'testing'
}
objectWithMethod.someMethod('yes-joh');
console.log(objectWithMethod.test123);

let newObject = Object.assign({},objectWithMethod,myObj,{test:889});
console.log(newObject);

```

## Data types

### set
New type of datastructure, used to store unique values of any type.
It has the folowing methods.

* set.size()
* add()
* delete()
* has()
* clear()
*
* foreach(fn)
* entries()

```javascript
 
    let mySet = new Set();

    let anArray = [1,1,2,3,3,3,3,3,4,5];
    let otherSet = new Set(anArray);

    console.log(otherSet,otherSet.size);
    otherSet.add(9).add(12);
    otherSet.delete(3);
    otherSet.has(1);// bool
    // otherSet.clear();

    otherSet.forEach(function(val1,val2,set){
        //val1 = val;
        //val2 should be the index but in the set it is also val1
        console.log(val1,val2,set);
    });


```

### maps
Looks like set but with key value pairs

* .size()
* add()
* delete()
* has()
* clear()
* get()
*   
* foreach(fn)
* keys()
* entries()
* values()

```javascript
    
     var map = new Map();
     
     let a = [3,4];
     let map2 = new Map([ ['key_','val_'],['name','daan'],[1,1], a ]) ;
     map2.set('test', {t:'est'});
     map2.get('naam');
     map2.has('key_');
 
 
     console.log(map2,map2.get('name'));
```

### WeakMaps and WeakSets
Same as sets and maps but references inside are weakly so they do not prevent garbage collection in case there would be no other reference to the object.

WeakSets can only be composed of objects.
WeakMaps can only have a object as key 

##### They can't be iterated!!

WeakSets
* add
* delete
* has

WeakMap
* delete 
* get
* has
* set

```javascript

var myWeakSet = new WeakSet();
myWeakSet.add({a:'a'});

var myWeakMap = new WeakMap();

```

### iterators

Any object with  and Next method

// * foreach
* entries (return an iterator object wit the next method, with val = index val pair)
* values (return an iterator object wit the next method, with val only value )
* keys (return an iterator object wit the next method, with val only key )
* next (method witch returns an object with value and done property; the value exists of [key,val] )

### for of loop
Same as for in loop but with the val in stead of the key

You can use it with

* arrays
* sets
* maps

```javascript
for (let val of ['a','b','c']){
    console.log(val);
}
```

### Arrow functions aka Fat Arrow functions
Syntactic sugar for function expressions and also use a lexical this

```javascript

var twice = (param1) => {
    return param1 + ' ' + param1; 
}
=======
var twice = param1 => {
    return param1 + ' ' + param1; 
}
=======
var twice = param1 => param1 + ' ' + param1; 


console.log(twice('echo'));
```

### generator functions
Declare a function with an *
They do not run to completon, they can return a value, pause and then do somthing else again.
This is not good transpiled by babel, it needs the polyfill
```javascript

function* vierkant(x){
    yeald x * x;
    yeald 'vierkant';
}
let vierkantGen = vierkant(5);
console.log( vierkantGen.next() );


```

### Promises

```javascript

var myPromise = new Promise(function(resolve_fn, reject_fn){
    resolve_fn('gelukt');
    // reject_fn('mislukt')'
});

myPromise.then(function(msg){
    console.log(msg);
    return true;
},function(msg){
    console.log(msg);
    return false;
});


```

### Classes
The same as prototypal inharitance.
Only the syntax changes.

```javascript

(function(){
    class Person {
        constructor(name){
            this.name = name;
        }
        sayHello() {
            return `hello, my name is ${this.name}, whats yours?`;
        }
        static upperName (pers){
            pers.name = pers.name.toUpperCase();
        }
    };
    let daan = new Person('Daan');

    console.log(daan.sayHello());
    Person.upperName(daan);
    console.log(daan.sayHello());

    class Father extends Person{
        sayHello() {
            return super.sayHello() + ' im your father?';
        }
    };
    let daanPa = new Father('Vaderrrr');
    console.log(daanPa.sayHello());
    Father.upperName(daanPa);
    console.log(daanPa.sayHello());

})();

```

### destructuring arrays and objects

(http://www.benmvp.com/learning-es6-destructuring/)


```javascript

var [x,y] = ['a','b'];
    console.log(x,y);
    let {a,b} = {a:'c',b:'d'}
    console.log(a,b);

    let {name,status:{staat}} = {
        name : "Daan Schaeffer",
        status : {
            nationaliteit : "nederlandsche",
            staat : "samenwonend"
        }
    }
    console.log(name, staat); // Daan Schaeffer samenwonend
    
    or if you are not declaring but only assigning wrap the statement in parenthesesis
    ([ x,y ] = ['x','y'])
    
    
```