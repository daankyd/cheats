module.exports = function(grunt) {
    "use strict";

    // Project configuration.
    grunt.initConfig({
        babel:{
            options: {
                sourceMap: true
            },
            dist: {
                files:{
                    'dist/js/es5.js': 'js/es6.js'
                }
            }
        }
    });


    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-babel');

    // Default task(s).
    grunt.registerTask('default', ['babel']);
};